#pragma once

#include <string>
#include "..\Common\DeviceResources.h"
#include "..\Common\StepTimer.h"

namespace Test4k
{
	// Renders the current FPS value in the bottom right corner of the screen using Direct2D and DirectWrite.
	class ImageRenderer
	{
	public:
		ImageRenderer(const std::shared_ptr<DX::DeviceResources>& deviceResources);
		void CreateDeviceDependentResources();
		void ReleaseDeviceDependentResources();
		void Update(DX::StepTimer const& timer);
		void Render();
		void Output(const wchar_t* szFormat, ...);
		D2D1_SIZE_U										m_imageSize;

	private:
		// Cached pointer to device resources.
		std::shared_ptr<DX::DeviceResources> m_deviceResources;

		// Resources related to image rendering.
		Microsoft::WRL::ComPtr<ID2D1DrawingStateBlock1> m_stateBlock;
		Microsoft::WRL::ComPtr<ID2D1Bitmap>             m_image;

	};
}