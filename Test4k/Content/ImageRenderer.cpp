#include "pch.h"
#include "ImageRenderer.h"

#include "Common/DirectXHelper.h"

using namespace Test4k;
using namespace Microsoft::WRL;
using namespace Windows::UI::Core;
using namespace Windows::ApplicationModel;
using namespace Windows::Foundation;
using namespace Windows::Storage;
using namespace Windows::UI::ViewManagement;
using namespace Windows::Graphics::Display;
using namespace D2D1;

// Initializes D2D resources used for image rendering.
ImageRenderer::ImageRenderer(const std::shared_ptr<DX::DeviceResources>& deviceResources) :
	m_deviceResources(deviceResources)
{

	DX::ThrowIfFailed(
		m_deviceResources->GetD2DFactory()->CreateDrawingStateBlock(&m_stateBlock)
	);

	CreateDeviceDependentResources();
}

// Updates the image to be displayed.
void ImageRenderer::Update(DX::StepTimer const& timer)
{

}

void ImageRenderer::Output(const wchar_t* szFormat, ...)
{
	WCHAR szBuff[1024];
	va_list arg;
	va_start(arg, szFormat);
	_vsnwprintf_s(szBuff, sizeof(szBuff), szFormat, arg);
	va_end(arg);

	OutputDebugString(szBuff);
}

// Renders a frame to the screen.
void ImageRenderer::Render()
{
	ID2D1DeviceContext* context = m_deviceResources->GetD2DDeviceContext();
	Windows::Foundation::Size logicalSize = m_deviceResources->GetLogicalSize();
	context->SaveDrawingState(m_stateBlock.Get());
	context->BeginDraw();
	
	context->DrawBitmap(
		m_image.Get(),
		D2D1::RectF(
			0,
			0,
			context->GetSize().width,
			context->GetSize().height
		)
	);

	// Ignore D2DERR_RECREATE_TARGET here. This error indicates that the device
	// is lost. It will be handled during the next call to Present.
	HRESULT hr = context->EndDraw();
	if (hr != D2DERR_RECREATE_TARGET)
	{
		DX::ThrowIfFailed(hr);
	}

	context->RestoreDrawingState(m_stateBlock.Get());
}

void ImageRenderer::CreateDeviceDependentResources()
{
	auto location = Package::Current->InstalledLocation;
	Platform::String^ path = Platform::String::Concat(location->Path, "\\");
	path = Platform::String::Concat(path, "Assets\\test.png");
	//path = Platform::String::Concat(path, "Assets\\1080.png");
	auto wicFactory = m_deviceResources->GetWicImagingFactory();

	ComPtr<IWICBitmapDecoder> wicBitmapDecoder;
	DX::ThrowIfFailed(
		wicFactory->CreateDecoderFromFilename(
			path->Data(),
			nullptr,
			GENERIC_READ,
			WICDecodeMetadataCacheOnDemand,
			&wicBitmapDecoder
		)
	);

	ComPtr<IWICBitmapFrameDecode> wicBitmapFrame;
	DX::ThrowIfFailed(
		wicBitmapDecoder->GetFrame(0, &wicBitmapFrame)
	);

	ComPtr<IWICFormatConverter> wicFormatConverter;
	DX::ThrowIfFailed(
		wicFactory->CreateFormatConverter(&wicFormatConverter)
	);

	DX::ThrowIfFailed(
		wicFormatConverter->Initialize(
			wicBitmapFrame.Get(),
			GUID_WICPixelFormat32bppPBGRA,
			WICBitmapDitherTypeNone,
			nullptr,
			0.0,
			WICBitmapPaletteTypeCustom  // The BGRA format has no palette so this value is ignored.
		)
	);

	double dpiX = 96.0f;
	double dpiY = 96.0f;
	DX::ThrowIfFailed(
		wicFormatConverter->GetResolution(&dpiX, &dpiY)
	);

	auto d2dContext = m_deviceResources->GetD2DDeviceContext();
	//d2dContext->GetPixelSize

	// Create D2D Resources
	DX::ThrowIfFailed(
		d2dContext->CreateBitmapFromWicBitmap(
			wicFormatConverter.Get(),
			BitmapProperties(
				PixelFormat(DXGI_FORMAT_B8G8R8A8_UNORM, D2D1_ALPHA_MODE_PREMULTIPLIED),
				static_cast<float>(dpiX),
				static_cast<float>(dpiY)
			),
			&m_image
		)
	);

	m_imageSize = m_image->GetPixelSize();

	Output(L"D2D WIDTH: %.0f\r\n", d2dContext->GetSize().width);
	Output(L"D2D HEIGHT: %.0f\r\n", d2dContext->GetSize().height);
	Output(L"IMAGE WIDTH: %.0f\r\n", m_image->GetSize().width);
	Output(L"IMAGE HEIGHT: %.0f\r\n", m_image->GetSize().height);
}

void ImageRenderer::ReleaseDeviceDependentResources()
{

}