﻿#include "pch.h"
#include "SampleFpsTextRenderer.h"
#include "ImageRenderer.h"

#include "Common/DirectXHelper.h"

using namespace Test4k;
using namespace Microsoft::WRL;

// Initializes D2D resources used for text rendering.
SampleFpsTextRenderer::SampleFpsTextRenderer(const std::shared_ptr<DX::DeviceResources>& deviceResources, const std::shared_ptr<ImageRenderer>& imageRenderer) :
	m_text(L""),
	m_deviceResources(deviceResources),
	m_imageRenderer(imageRenderer)
{
	ZeroMemory(&m_textMetrics, sizeof(DWRITE_TEXT_METRICS));

	// Create device independent resources
	ComPtr<IDWriteTextFormat> textFormat;
	DX::ThrowIfFailed(
		m_deviceResources->GetDWriteFactory()->CreateTextFormat(
			L"Segoe UI",
			nullptr,
			DWRITE_FONT_WEIGHT_NORMAL,
			DWRITE_FONT_STYLE_NORMAL,
			DWRITE_FONT_STRETCH_NORMAL,
			24.0f,
			L"en-US",
			&textFormat
			)
		);

	DX::ThrowIfFailed(
		textFormat.As(&m_textFormat)
		);

	DX::ThrowIfFailed(
		m_textFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_LEADING)
		);

	DX::ThrowIfFailed(
		m_deviceResources->GetD2DFactory()->CreateDrawingStateBlock(&m_stateBlock)
		);

	CreateDeviceDependentResources();
}

// Updates the text to be displayed.
void SampleFpsTextRenderer::Update(DX::StepTimer const& timer)
{
	// Update display text.
	uint32 fps = timer.GetFramesPerSecond();
	ID2D1DeviceContext* context = m_deviceResources->GetD2DDeviceContext();

	m_text = (fps > 0) ? std::to_wstring(fps) + L" FPS\r\n" : L" - FPS\r\n";
	std::wstring txt = L"";
	txt += L"IMAGE WIDTH: " + std::to_wstring(lround(m_imageRenderer->m_imageSize.width)) + L"px\r\n";
	txt += L"IMAGE HEIGHT: " + std::to_wstring(lround(m_imageRenderer->m_imageSize.height)) + L"px\r\n";
	txt += L"LOGICAL WIDTH: " + std::to_wstring(context->GetSize().width) + L"px\r\n";
	txt += L"LOGICAL HEIGHT: " + std::to_wstring(context->GetSize().height) + L"px\r\n";
	txt += L"OUTPUT WIDTH: " + std::to_wstring(lround(m_deviceResources->GetOutputSize().Width)) + L"px\r\n";
	txt += L"OUTPUT HEIGHT: " + std::to_wstring(lround(m_deviceResources->GetOutputSize().Height)) + L"px\r\n";

	//txt += L"PIXEL WIDTH:" + std::to_wstring(lround(m_deviceResources->))
	m_text = m_text + txt;

	ComPtr<IDWriteTextLayout> textLayout;
	DX::ThrowIfFailed(
		m_deviceResources->GetDWriteFactory()->CreateTextLayout(
			m_text.c_str(),
			(uint32) m_text.length(),
			m_textFormat.Get(),
			context->GetSize().width - 32.0f, // Max width of the input text.
			context->GetSize().height - 32.0f, // Max height of the input text.
			&textLayout
			)
		);

	DX::ThrowIfFailed(
		textLayout.As(&m_textLayout)
		);

	DX::ThrowIfFailed(
		m_textLayout->GetMetrics(&m_textMetrics)
		);
}

// Renders a frame to the screen.
void SampleFpsTextRenderer::Render()
{
	ID2D1DeviceContext* context = m_deviceResources->GetD2DDeviceContext();
	Windows::Foundation::Size logicalSize = m_deviceResources->GetLogicalSize();

	context->SaveDrawingState(m_stateBlock.Get());
	context->BeginDraw();

	// Position on the bottom right corner
	D2D1::Matrix3x2F screenTranslation = D2D1::Matrix3x2F::Translation(
		32.0f,
		32.0f
		);

	context->SetTransform(screenTranslation * m_deviceResources->GetOrientationTransform2D());

	DX::ThrowIfFailed(
		m_textFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_LEADING)
		);

	context->DrawTextLayout(
		D2D1::Point2F(0.f, 0.f),
		m_textLayout.Get(),
		m_whiteBrush.Get()
		);

	// Ignore D2DERR_RECREATE_TARGET here. This error indicates that the device
	// is lost. It will be handled during the next call to Present.
	HRESULT hr = context->EndDraw();
	if (hr != D2DERR_RECREATE_TARGET)
	{
		DX::ThrowIfFailed(hr);
	}

	context->RestoreDrawingState(m_stateBlock.Get());
}

void SampleFpsTextRenderer::CreateDeviceDependentResources()
{
	DX::ThrowIfFailed(
		m_deviceResources->GetD2DDeviceContext()->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::White), &m_whiteBrush)
		);
}
void SampleFpsTextRenderer::ReleaseDeviceDependentResources()
{
	m_whiteBrush.Reset();
}